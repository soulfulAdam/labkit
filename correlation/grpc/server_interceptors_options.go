package grpccorrelation

// The configuration for server correlation interceptors.
type serverInterceptConfig struct {
	propagateIncomingCorrelationID bool
	reversePropagateCorrelationID  bool
}

// ServerCorrelationInterceptorOption configures server correlation interceptor.
type ServerCorrelationInterceptorOption func(*serverInterceptConfig)

func applyServerCorrelationInterceptorOptions(opts []ServerCorrelationInterceptorOption) serverInterceptConfig {
	config := serverInterceptConfig{
		propagateIncomingCorrelationID: true, // enabled by default
	}
	for _, v := range opts {
		v(&config)
	}

	return config
}

// WithoutPropagation disables correlation id propagation from incoming request metadata.
// If the id is missing or the interceptor is configured to not propagate it, a new id is generated and
// injected into the request context.
func WithoutPropagation() ServerCorrelationInterceptorOption {
	return func(config *serverInterceptConfig) {
		config.propagateIncomingCorrelationID = false
	}
}

// WithReversePropagation enables server -> client correlation id propagation via response metadata.
// Client can then use the returned correlation id e.g. for logging purposes.
// It only makes sense to use this option together with WithoutPropagation i.e. in situations, when
// client-supplied correlation id is not trusted so server generates its own one and hence clients doesn't have it.
func WithReversePropagation() ServerCorrelationInterceptorOption {
	return func(config *serverInterceptConfig) {
		config.reversePropagateCorrelationID = true
	}
}
