package raven_test

import (
	"context"
	"fmt"

	raven "github.com/getsentry/raven-go"
	correlation "gitlab.com/gitlab-org/labkit/correlation/raven"
)

func Example() {
	// In reality, this would be passed into the function
	ctx := context.Background()
	err := fmt.Errorf("An error occurred")

	client := raven.DefaultClient
	extra := correlation.SetExtra(ctx, nil)
	packet := raven.NewPacketWithExtra(fmt.Sprintf("error: %v", err), extra)
	client.Capture(packet, nil)
}
