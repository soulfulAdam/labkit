module gitlab.com/gitlab-org/labkit

require (
	cloud.google.com/go/profiler v0.1.0
	cloud.google.com/go/trace v0.1.0 // indirect
	contrib.go.opencensus.io/exporter/stackdriver v0.13.8
	github.com/HdrHistogram/hdrhistogram-go v1.1.1 // indirect
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/certifi/gocertifi v0.0.0-20210507211836-431795d63e8d // indirect
	github.com/client9/reopen v1.0.0
	github.com/getsentry/raven-go v0.2.0
	github.com/getsentry/sentry-go v0.11.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/lightstep/lightstep-tracer-go v0.25.0
	github.com/oklog/ulid/v2 v2.0.2
	github.com/opentracing/opentracing-go v1.2.0
	github.com/philhofer/fwd v1.1.1 // indirect
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/client_model v0.2.0
	github.com/sebest/xff v0.0.0-20210106013422-671bd2870b3a
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	go.opencensus.io v0.23.0
	google.golang.org/api v0.54.0
	google.golang.org/grpc v1.40.0
	gopkg.in/DataDog/dd-trace-go.v1 v1.32.0
)

go 1.16
