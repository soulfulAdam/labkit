package impl

import "errors"

// ErrConfiguration is the base exception for tracing configuration errors.
var ErrConfiguration = errors.New("jaeger tracer: configuration error")
